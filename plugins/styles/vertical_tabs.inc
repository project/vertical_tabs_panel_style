<?php

/**
 * @file
 */

// Plugin definition
$plugin = array(
  'title' => t('Vertical tabs'),
  'description' => t('Presents the panes in vertical tabs.'),
  'render region' => 'vertical_tabs_panel_style_render_region',
  'settings form' => 'vertical_tabs_panel_style_settings_form',
  'defaults' => array(
    'animate' => 0,
    'timeout' => 5000,
    'speed' => 700,
    'pause' => 1,
    'title' => '',
  ),
);

function theme_vertical_tabs_panel_style_render_region($display, $owner_id, $panes, $settings, $region_id) {
  $config = array();
  $output = '';
  $css_id = form_clean_id('vertical-tabs-' . $region_id);
  
  if ($settings['title']) {
    $output .= '<h2 class="title">' . $settings['title'] . '</h2>';
  }
  
  $form = array(
    '#prefix' => '<div id="' . $css_id . '">',
    '#suffix' => '</div>',
  );
  
  foreach ($panes as $pane_id => $content) {
    $pane = $display->content[$pane_id];
    $key = $pane->type . '-' . $pane->pid;
    
    $config[] = $key;
    
    $form[$key] = array(
      '#type' => 'fieldset',
      '#title' => $pane->title,
      'content' => array(
        '#value' => $content,
      ),
      '#attributes' => array('class' => 'vertical-tab-pane-style'),
    );
  }
  
  vertical_tabs_add_vertical_tabs($form, $config);

  $js_settings = array(
    'verticalTabsPaneStyle' => array(
      $css_id => $settings,
    ),
  );
  drupal_add_js($js_settings, 'setting');
  drupal_add_js(drupal_get_path('module', 'vertical_tabs_panel_style') . '/js/vertical_tabs_animate.js', 'footer');
  drupal_add_js(drupal_get_path('module', 'vertical_tabs_panel_style') . '/js/jquery.cycle.all.min.js');

  $output .= drupal_render($form);
  
  $js = <<<EOL
<script type="text/javascript">
<!--//--><![CDATA[//><!--
$('.vertical-tab-pane-style')
  .css({display: 'none'});
//--><!]]>
</script>
EOL;

  $output .= $js;
  return $output;
}

function vertical_tabs_panel_style_settings_form($style_settings) {
  $form = array();
  
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Panel Title'),
    '#default_value' => $style_settings['title'],
    '#description' => t('Display title above the vertical tabs.'),
  );
  
  $form['animate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Animate'),
    '#default_value' => $style_settings['animate'],
  );
  
  $form['timeout'] = array(
    '#type' => 'textfield',
    '#title' => t('Timer delay'),
    '#default_value' => $style_settings['timeout'],
    '#description' => t('Amount of time in milliseconds between transitions. Set the value to 0 to not rotate the slideshow automatically.')
  );
  
  $form['speed'] = array(
    '#type' => 'textfield',
    '#title' => t('Speed'),
    '#default_value' => $style_settings['speed'],
    '#description' => t('Time in milliseconds that each transition lasts. Numeric only!'),
  );
  
  $form['pause'] = array(
    '#type' => 'radios',
    '#title' => t('Pause'),
    '#default_value' => $style_settings['pause'],
    '#options' => array(1 => t('Yes'), 0 => t('No')),
    '#description' => t('Pause when hovering on the vertical tabs'),
  );
  
  $form['effect'] = array(
    '#type' => 'select',
    '#title' => t('Effect'),
    '#default_value' => $style_settings['effect'],
    '#options' => array(
      'none' => t('None'),
      'blindX' => t('blindX'),
      'blindY' => t('blindY'),
      'blindZ' => t('blindZ'),
      'cover' => t('cover'),
      'curtainX' => t('curtainX'),
      'curtainY' => t('curtainY'),
      'fade' => t('fade'),
      'fadeZoom' => t('fadeZoom'),
      'growX' => t('growX'),
      'growY' => t('growY'),
      'scrollUp' => t('scrollUp'),
      'scrollDown' => t('scrollDown'),
      'scrollLeft' => t('scrollLeft'),
      'scrollRight' => t('scrollRight'),
      'scrollHorz' => t('scrollHorz'),
      'scrollVert' => t('scrollVert'),
      'shuffle' => t('shuffle'),
      'slideX' => t('slideX'),
      'slideY' => t('slideY'),
      'toss' => t('toss'),
      'turnUp' => t('turnUp'),
      'turnDown' => t('turnDown'),
      'turnLeft' => t('turnLeft'),
      'turnRight' => t('turnRight'),
      'uncover' => t('uncover'),
      'wipe' => t('wipe'),
      'zoom' => t('zoom'),
    ),
    '#description' => t('The transition effect that will be used to change between images. Not all options below may be relevant depending on the effect.'),
  );
  
  return $form;
}