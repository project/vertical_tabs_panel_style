<?php

/**
 * @file
 */

$plugin = array(
  'title' => t('No title'),
  'description' => t('Display the body of the content with no title'),
  'render pane' => 'vertical_tabs_panel_style_no_title',
);

function theme_vertical_tabs_panel_style_no_title($content, $pane, $display) {
  return '<div>' . $content->content . '</div>';
}