/**
 * @file
 */
(function ($) {
  Drupal.behaviors.verticalTabsZAnimate = function ($context) {
    $.each(Drupal.settings.verticalTabsPaneStyle, function (k, v) {
      if (v.animate) {
        $('#' + k + ' .vertical-tabs-panes').cycle(
          {
            'timeout': v.timeout,
            'speed': v.speed,
            'fx': v.effect,
            'after': function () {
              $('#' + k + ' .vertical-tab-button').removeClass('selected');
              classes = $(this).attr('class').split(' ');
              tab_class = classes[0].replace('vertical-tabs-', 'vertical-tabs-list-');
              $('#' + k + ' .vertical-tab-button .' + tab_class)
                .parent()
                .addClass('selected');
            }
          }
        );
        $('#' + k + ' .vertical-tab-button a').click(
          function () {
            $('#' + k + ' .vertical-tabs-panes').cycle('stop');
            classes = $(this).attr('class').split(' ');
            tab_class = classes[0].replace('vertical-tabs-list-', 'vertical-tabs-');
            $('#' + k + ' .vertical-tabs-panes .' + tab_class).removeAttr('style');
          }
        );
        if (v.pause) {
          $('#' + k).hover(
            function () {
              $('#' + k + ' .vertical-tabs-panes').cycle('pause');
            },
            function () {
              $('#' + k + ' .vertical-tabs-panes').cycle('resume');
            }
          );
        }
      }
    });
  }
})(jQuery);